import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit, OnDestroy {

  currentRouteTitle = '';

  goBack = '';

  routerSubscriber: Subscription;

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.checkCurrentRoute();

    this.routerSubscriber = this.router
      .events
      .subscribe(v => {

        this.checkCurrentRoute();

      })

  }

  ngOnDestroy() {
    this.routerSubscriber.unsubscribe();
  }

  checkCurrentRoute() {
    if (this.activatedRoute.snapshot.firstChild.data && this.activatedRoute.snapshot.firstChild.data.pageTitle) {
      this.currentRouteTitle = this.activatedRoute.snapshot.firstChild.data.pageTitle;
    }

    if (this.activatedRoute.snapshot.firstChild.data && this.activatedRoute.snapshot.firstChild.data.goBack) {
      this.goBack = this.activatedRoute.snapshot.firstChild.data.goBack;
    } else {
      this.goBack = '';
    }
  }

  goNavBack() {
    if (this.goBack) {
      this.router.navigate([this.goBack])
    }
  }

  logout() {
    this.router.navigate(['/login']);
  }

}
