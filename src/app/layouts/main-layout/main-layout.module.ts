import {NgModule} from '@angular/core';
import {MainLayoutComponent} from './main-layout.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {LoginPageComponent} from '../../pages/login-page/login-page.component';
import {BoardsPageComponent} from '../../pages/boards-page/boards-page.component';
import {BoardDetailPageComponent} from '../../pages/board-detail-page/board-detail-page.component';

const routes: Routes = [
  {path: '', component: MainLayoutComponent},
  {path: 'all', component: BoardsPageComponent, data: {pageTitle: 'Boards'}, pathMatch: 'full'},
  {path: 'detail', component: BoardDetailPageComponent, data: {pageTitle: 'Board detail', goBack: '/boards'}, pathMatch: 'full'}
];

@NgModule({
  imports: [
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    RouterModule,
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MainLayoutComponent
  ]
})

export class MainLayoutModule {
}
