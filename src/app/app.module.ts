import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { BoardsPageComponent } from './pages/boards-page/boards-page.component';
import { BoardDetailPageComponent } from './pages/board-detail-page/board-detail-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import {FormsModule} from '@angular/forms';
import { LoadingComponent } from './components/loading/loading.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { BoardItemComponent } from './components/board-item/board-item.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import {NewBoardDialogComponent} from './pages/boards-page/components/new-board-dialog.component.ts/new-board-dialog.component';
import {ColorPickerModule} from 'ngx-color-picker';
import {MainLayoutModule} from './layouts/main-layout/main-layout.module';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { TaskItemComponent } from './components/task-item/task-item.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import { ColumnItemComponent } from './components/column-item/column-item.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    BoardsPageComponent,
    BoardDetailPageComponent,
    LoadingComponent,
    BoardItemComponent,
    NewBoardDialogComponent,
    TaskItemComponent,
    ColumnItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatMenuModule,
    MatExpansionModule,
    FormsModule,
    ColorPickerModule,
    MainLayoutModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    NewBoardDialogComponent
  ]
})
export class AppModule { }
