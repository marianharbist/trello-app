import { Injectable } from '@angular/core';
import {BoardModel} from '../models/board.model';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public STORAGE_NAME = 'boards';

  dataChanged: Subject<BoardModel[]> = new Subject<BoardModel[]>();

  constructor() { }

  save(data: BoardModel[]) {
    localStorage.setItem(this.STORAGE_NAME, JSON.stringify(data));
    this.dataChanged.next(data);
  }

  load(): BoardModel[] | null {
    const data = localStorage.getItem(this.STORAGE_NAME);

    if (data) {
      return JSON.parse(data)
    }

    return null;
  }
}
