import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  isLoading = false;
  _loadingChange: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  loadingOn() {
    this.isLoading = true;
    this._loadingChange.next(this.isLoading);
  }

  loadingOff() {
    this.isLoading = false;
    this._loadingChange.next(this.isLoading);
  }
}
