import {TaskModel} from './task.model';

export class ColumnModel {
  title: string;
  tasks: TaskModel[];
}
