import {ColumnModel} from './column.model';

export class BoardModel {
  title: string;
  backgroundColor: string;
  columns: ColumnModel[];

}
