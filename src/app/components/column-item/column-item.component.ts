import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ColumnModel} from '../../models/column.model';
import {TaskModel} from '../../models/task.model';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {StorageService} from '../../services/storage.service';
import {BoardModel} from '../../models/board.model';
import {ActivatedRoute} from '@angular/router';
import {Task} from 'protractor/built/taskScheduler';

@Component({
  selector: 'app-column-item',
  templateUrl: './column-item.component.html',
  styleUrls: ['./column-item.component.scss']
})
export class ColumnItemComponent implements OnInit {

  isEditing = false;

  editedTitle = '';

  @Input() column: ColumnModel;

  @Input() board: BoardModel;

  @Output() onDeleteColumn: EventEmitter<ColumnModel> = new EventEmitter<ColumnModel>();

  @Output() columnChanged: EventEmitter<ColumnModel> = new EventEmitter<ColumnModel>();

  constructor(
    protected storage: StorageService,
    protected activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {

    if (this.column && !this.column.title) {
      this.isEditing = true;
    }

  }

  addNewTask(col: ColumnModel) {
    if (!col.tasks) {
      col.tasks = [];
    }
    col.tasks.push(new TaskModel())
  }

  deleteTask(task: TaskModel, index: number) {
    this.column.tasks.splice(index, 1);
    this.columnChanged.emit(this.column);
  }

  drop(event: CdkDragDrop<string[]>, column: ColumnModel) {
    moveItemInArray(column.tasks, event.previousIndex, event.currentIndex);
    this.columnChanged.emit(this.column);
  }

  deleteColumn() {
    this.onDeleteColumn.emit(this.column);
    this.columnChanged.emit(this.column);
  }

  toggleEditColumn() {
    this.isEditing = true;
    this.editedTitle = this.column.title;
  }

  saveEdit() {
    if (this.editedTitle && this.editedTitle.length >= 3) {
      this.column.title = this.editedTitle;
      this.columnChanged.emit(this.column);
    }
    this.closeEdit();
  }

  closeEdit() {
    if (!this.column.title) {
      this.deleteColumn();
    }
    this.isEditing = false;
  }

  onTaskChanged(task: TaskModel, index: number) {
    this.columnChanged.emit(this.column);
  }
}
