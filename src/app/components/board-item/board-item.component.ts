import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BoardModel} from '../../models/board.model';

@Component({
  selector: 'app-board-item',
  templateUrl: './board-item.component.html',
  styleUrls: ['./board-item.component.scss']
})
export class BoardItemComponent implements OnInit {

  @Input() board: BoardModel;

  @Output() onDeleteBoard: EventEmitter<BoardModel> = new EventEmitter<BoardModel>();

  @Output() onBoardClick: EventEmitter<BoardModel> = new EventEmitter<BoardModel>();

  constructor() { }

  ngOnInit(): void {
  }

  deleteBoard(e: Event) {
    e.preventDefault();
    this.onDeleteBoard.emit(this.board);
  }

  onClick(e: Event) {
    this.onBoardClick.emit(this.board);
  }

}
