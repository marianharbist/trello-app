import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoadingService} from '../../services/loading.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {
  showLoading = false;

  loadingChange: Subscription;

  constructor(
    protected loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.loadingChange = this.loadingService
      ._loadingChange
      .subscribe(v => this.showLoading = v);
  }

  ngOnDestroy() {
    if (this.loadingChange) {
      this.loadingChange.unsubscribe();
    }
  }

}
