import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaskModel} from '../../models/task.model';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit {
  isEditing = false;

  editedTitle = '';

  @Input() task: TaskModel

  @Output() onDelete: EventEmitter<TaskModel> = new EventEmitter<TaskModel>();

  @Output() taskChanged: EventEmitter<TaskModel> = new EventEmitter<TaskModel>();

  constructor() { }

  ngOnInit(): void {
    if (this.task && !this.task.title) {
      this.isEditing = true;
    }
  }

  editTask() {
    this.isEditing = true;
    this.editedTitle = this.task.title;
  }

  saveEdit() {
    if (this.editedTitle && this.editedTitle.length >= 3) {
      this.task.title = this.editedTitle;
    }
    this.closeEdit();
  }

  closeEdit() {
    if (!this.task.title) {
      this.deleteTask();
    }
    this.taskChanged.emit(this.task);
    this.isEditing = false;
  }

  deleteTask() {
    this.onDelete.emit(this.task);
    this.taskChanged.emit(this.task);
  }

}
