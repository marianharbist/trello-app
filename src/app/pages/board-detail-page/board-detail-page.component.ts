import { Component, OnInit } from '@angular/core';
import {BoardModel} from '../../models/board.model';
import {ColumnModel} from '../../models/column.model';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-board-detail-page',
  templateUrl: './board-detail-page.component.html',
  styleUrls: ['./board-detail-page.component.scss']
})
export class BoardDetailPageComponent implements OnInit {

  board: BoardModel = new BoardModel();

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected storage: StorageService,
    protected router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute
      .queryParams
      .subscribe(params => {
        if (params && params.board) {
          this.board = JSON.parse(params.board);
        }
      })
  }

  addNewColumn() {
    if (!this.board.columns) {
      this.board.columns = [];
    }
    this.board.columns.push(new ColumnModel());
  }

  deleteColumn(column: ColumnModel, index: number) {
    this.board.columns.splice(index, 1);
    this.saveChanges(column, index, true);
  }

  saveChanges(column: ColumnModel, colIndex: number, replaceColumns = false) {
    const data = this.storage.load();
    if (data) {
      const boardIndex = data.findIndex(b => b.title === this.board.title);

      if (boardIndex !== -1) {

        if (replaceColumns) {
          data[boardIndex].columns = this.board.columns;
        } else {
          if (!data[boardIndex].columns) {
            data[boardIndex].columns = [];
          }
          data[boardIndex].columns[colIndex] = column;
        }
        this.router.navigate(['/board-detail'], {queryParams: {board: JSON.stringify(data[boardIndex])}})
        this.storage.save(data);
      }
    }
  }
}
