import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {BoardsPageComponent} from '../../boards-page.component';
import {BoardModel} from '../../../../models/board.model';

@Component({
  selector: 'app-new-board-dialog-overview',
  templateUrl: 'new-board-dialog.component.html',
  styleUrls: ['new-board-dialog.component.scss']
})
export class NewBoardDialogComponent {

  formIsValid = false;

  errorMsg = '';

  constructor(
    public dialogRef: MatDialogRef<BoardsPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {board: BoardModel, allBoards: BoardModel[]}
  ) {
    this.dialogRef.disableClose = true;
  }

  save(e: Event) {
    e.preventDefault();
    this.dialogRef.close(this.data.board);
  }

  close(e: Event) {
    e.preventDefault();
    this.dialogRef.close(null);
  }

  colorChanged() {
    this.checkFormValidity();
  }

  isValidColor(strColor: string){
    const regex = new RegExp('^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$');
    return regex.test(strColor);
  }

  isValidName(): boolean {
    const item = this.data.allBoards.filter((b: BoardModel) => b.title === this.data.board.title);
    if (item && item.length > 0) {
      this.errorMsg = 'Board with same title already exists.'
      return false;
    }
    this.errorMsg = '';
    return true;
  }

  checkFormValidity() {
    this.formIsValid = this.isValidName() && this.isValidColor(this.data.board.backgroundColor);
  }
}
