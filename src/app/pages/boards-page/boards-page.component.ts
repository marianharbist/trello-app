import {Component, OnInit} from '@angular/core';
import {BoardModel} from '../../models/board.model';
import {Router} from '@angular/router';
import {NewBoardDialogComponent} from './components/new-board-dialog.component.ts/new-board-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {stringify} from 'querystring';
import {StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-boards-page',
  templateUrl: './boards-page.component.html',
  styleUrls: ['./boards-page.component.scss']
})
export class BoardsPageComponent implements OnInit {

  boards: BoardModel[] = [];

  constructor(
    protected router: Router,
    public dialog: MatDialog,
    protected storageService: StorageService
  ) {
  }

  ngOnInit(): void {

    const data = this.storageService.load();

    if (data) {
      this.boards = data;
    }

  }

  mockData() {
    this.boards.push({
      title: 'Tabula 1',
      backgroundColor: '#303f9f',
      columns: [
        {
          title: 'TestCol',
          tasks: [
            {
              title: 'TestTask1'
            },
            {
              title: 'TestTask2'
            },
            {
              title: 'TestTask3'
            }
          ]
        }
      ]
    })

    this.boards.push({
      title: 'Test2',
      backgroundColor: '#c5cae9',
      columns: [
        {
          title: 'TestCol2',
          tasks: [
            {
              title: 'TestTask'
            }
          ]
        },
        {
          title: 'TestCol3',
          tasks: [
            {
              title: 'TestTask'
            },
            {
              title: 'TestTas2'
            },
            {
              title: 'TestTas3'
            }
          ]
        }
      ]
    })

    this.boards.push({
      title: 'Test3',
      backgroundColor: '#9fa8da',
      columns: [
        {
          title: 'TestCol',
          tasks: [
            {
              title: 'TestTask'
            }
          ]
        }
      ]
    })

    this.boards.push({
      title: 'Test',
      backgroundColor: '#7986cb',
      columns: [
        {
          title: 'TestCol',
          tasks: [
            {
              title: 'TestTask'
            }
          ]
        }
      ]
    })

    this.storageService.save(this.boards);
  }

  addNewBoard(e: Event) {
    e.preventDefault();
    this.openDialog()
  }

  onBoardClick(board: BoardModel) {
    this.router.navigate(['/board-detail'], {queryParams: {board: JSON.stringify(board)}});
  }

  openDialog(): void {
    const newBoard = new BoardModel();

    const dialogRef = this.dialog.open(NewBoardDialogComponent, {
      width: '250px',
      data: {board: newBoard, allBoards: this.boards}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result instanceof BoardModel && result.title && result.backgroundColor) {
        this.boards.push(result)
        this.storageService.save(this.boards);
      }
    });
  }

  deleteBoard(board: BoardModel, index: number) {
      this.boards.splice(index, 1);
      this.storageService.save(this.boards);
  }

}
