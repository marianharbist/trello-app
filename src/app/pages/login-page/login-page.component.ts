import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

const APP_CREDENTIALS = {
  username: 'trello@trello.com',
  password: 'trello123'
}

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginForm = {
    username: 'trello@trello.com',
    password: 'trello123'
  };

  constructor(
    private _snackBar: MatSnackBar,
    protected router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(e: Event) {
    e.preventDefault();
    if (this.loginForm.username === APP_CREDENTIALS.username && this.loginForm.password === APP_CREDENTIALS.password) {
      this._snackBar.open('Success', 'User logged in successfully.', {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
      });
      this.router.navigate(['/boards']);
      return;
    }
    this._snackBar.open('Error', 'Login error, please, check your credentials.', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
    return;
  }

}
