import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {BoardsPageComponent} from './pages/boards-page/boards-page.component';
import {BoardDetailPageComponent} from './pages/board-detail-page/board-detail-page.component';
import {MainLayoutComponent} from './layouts/main-layout/main-layout.component';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginPageComponent},
  {
    path: '', component: MainLayoutComponent, data: {test: 'test'}, children: [
      {path: 'boards', component: BoardsPageComponent, data: {pageTitle: 'Boards'}, pathMatch: 'full'},
      {path: 'board-detail', component: BoardDetailPageComponent, data: {pageTitle: 'Board detail', goBack: '/boards'}}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
